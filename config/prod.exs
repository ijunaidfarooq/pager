use Mix.Config

config :pager, Pager.Repo,
  adapter: Ecto.Adapters.Postgres,
  url: "postgres://jmezjcejvxhoao:c93b1c1adac9c576f63fd8454591d2350d96d044539466bfaa1b3240830eb472@ec2-54-235-242-63.compute-1.amazonaws.com:5432/df3gjgslfdnu4r",
  socket_options: [keepalive: true],
  timeout: 60_000,
  pool_timeout: 60_000,
  pool_size: 20,
  size: 20,
  lazy: false,
  ssl: true
