use Mix.Config

config :pager, Pager.Repo,
  adapter: Ecto.Adapters.Postgres,
  database: "pager_repo",
  username: "postgres",
  password: "postgres",
  hostname: "localhost"
