defmodule Pager.Application do
  @moduledoc false

  use Application

  def start(_type, _args) do
    import Supervisor.Spec, warn: false
    children = [
      supervisor(Pager.Repo, [])
    ]
    opts = [strategy: :one_for_one, name: Pager.Supervisor]
    Supervisor.start_link(children, opts)
  end
end
