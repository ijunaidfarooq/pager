defmodule Pager.Daemon.Server do
  use Ecto.Schema
  import Ecto.Changeset

  @ip_regex ~r/^(http(s?):\/\/)?(((www\.)?+[a-zA-Z0-9\.\-\_]+(\.[a-zA-Z]{2,3})+)|(\b(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\b))(\/[a-zA-Z0-9\_\-\s\.\/\?\%\#\&\=]*)?$/

  schema "servers" do
    field :frequency, :integer
    field :ip, :string
    field :name, :string
    field :password, :string
    field :ping, :boolean, default: false
    field :ports, :map
    field :raid, :boolean, default: false
    field :raid_errors, :boolean, default: false
    field :ssh, :boolean, default: false
    field :username, :string

    timestamps()
  end

  @doc false
  def changeset(server, attrs) do
    server
    |> cast(attrs, [:name, :ip, :username, :password, :ports, :raid, :ssh, :raid_errors, :ping, :frequency])
    |> validate_required([:name, :ip, :username, :password, :ports, :raid, :ssh, :raid_errors, :ping, :frequency])
    |> validate_length(:name, [min: 3, message: "Name should be at least 2 character(s)."])
    |> validate_length(:username, [min: 3, message: "Username should be at least 2 character(s)."])
    |> validate_length(:password, [min: 3, message: "Password should be at least 2 character(s)."])
    |> validate_format(:ip, @ip_regex, [message: "URL / IP format isn't valid!"])
  end
end
