defmodule Pager do
  @moduledoc """
  Documentation for Pager.
  """

  @doc """
  Hello world.

  ## Examples

      iex> Pager.hello()
      :world

  """
  def hello do
    :world
  end
end
