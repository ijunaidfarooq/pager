defmodule Pager.MixProject do
  use Mix.Project

  #ansible-playbook ~/pager/deploy/pager-playbook.yml --extra-vars "target=hotcode" --tags "deploy" -vvvv

  def project do
    [
      app: :pager,
      version: "1.0.#{DateTime.to_unix(DateTime.utc_now())}",
      elixir: "~> 1.7",
      start_permanent: Mix.env() == :prod,
      deps: deps()
    ]
  end

  # Run "mix help compile.app" to learn about applications.
  def application do
    [
      mod: {Pager.Application, []},
      extra_applications: [:logger, :distillery, :postgrex, :ecto]
    ]
  end

  # Run "mix help deps" to learn about dependencies.
  defp deps do
    [
      {:distillery, github: "bitwalker/distillery"},
      {:postgrex, "~> 0.13.5"},
      {:ecto, "~> 2.2"},
      {:phoenix, "~> 1.3", only: :dev}
    ]
  end
end
