defmodule Pager.Repo.Migrations.CreateServers do
  use Ecto.Migration

  def change do
    create table(:servers) do
      add :name, :string
      add :ip, :string
      add :username, :string
      add :password, :string
      add :ports, :map
      add :raid, :boolean, default: false, null: false
      add :ssh, :boolean, default: false, null: false
      add :raid_errors, :boolean, default: false, null: false
      add :ping, :boolean, default: false, null: false
      add :frequency, :integer

      timestamps()
    end

  end
end
